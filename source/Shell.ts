import {spawnSync, SpawnSyncOptionsWithStringEncoding} from 'child_process';

const [node = '', script = '', ...argumentList] = process.argv;

class Shell {
  // static readonly instance: Shell = new Shell();
  static readonly argumentList: string[] = argumentList;
  static readonly node: string = node;
  static readonly script: string = script;

  static run(command: string, environment: object = {}, silent: boolean = false): string | boolean {
    if ('' === command) {
      return false;
    }

    const options: SpawnSyncOptionsWithStringEncoding = {
      shell: true,
      encoding: 'utf8',
      maxBuffer: Number.MAX_SAFE_INTEGER,
      env: Object.assign({}, process.env, environment)
    };

    if (false === silent) {
      options.stdio = 'inherit';
      console.log(command);
    }

    try {
      const {status, stdout, stderr} = spawnSync(command, [], options);

      if (0 === status) {
        return (null === stdout) ? '' : stdout.trim();
      }

      if (null !== stderr) {
        console.log(stderr);
      }

      return false;
    } catch (execption) {
      if (execption instanceof Error) {
        console.log(execption.message);
      }

      return false;
    }
  }
}

export default Shell;
