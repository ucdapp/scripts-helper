import {inspect, InspectOptions} from 'util';

class ObjectInspector {
  static readonly instance: ObjectInspector = new ObjectInspector();
  static options: InspectOptions = {};

  protected constructor() {
  }

  static dir(item: any, options: InspectOptions = ObjectInspector.options) {
    const text = ObjectInspector.wrap(item, options);
    return console.log(text);
  }

  static getInspectFunction(item: object): Function | null {
    if (null === item) {
      return null;
    }

    const custom = inspect.custom;
    const index = Object.getOwnPropertySymbols(item).findIndex(value => value === custom);
    return (0 > index) ? null : (<any>item)[custom];
  }

  static wrap(item: any, options: InspectOptions = ObjectInspector.options) {
    const instance = ObjectInspector.instance;
    const bind = instance.bindInspectFunction;
    const oldFunction = bind(item, (<any>instance)[inspect.custom]);
    const text = inspect(item, options);
    bind(item, oldFunction);
    return text;
  }

  // noinspection JSMethodCanBeStatic
  bindInspectFunction(item: any, inspectFunction: Function | null): Function | null {
    if (('object' !== typeof item) || (null === item)) {
      return null;
    }

    const custom = inspect.custom;
    const oldFunction = ObjectInspector.getInspectFunction(item);

    if ((null !== inspectFunction) && (null === oldFunction)) {
      item[custom] = inspectFunction.bind(item);
    }

    if ((null === inspectFunction) && (null !== oldFunction)) {
      delete item[custom];
    }

    return oldFunction;
  }

  [inspect.custom](depth: number | null, options: any) {
    if ((null !== depth) && (0 > depth)) {
      return options.stylize('<MaxDepth>', 'special');
    }

    const newDepth = (null === options.depth) ? null : options.depth - 1;
    const newOptions = Object.assign({}, options, {depth: newDepth});
    const isArray = this instanceof Array;
    const replace = '\n  ';
    const items: string[] = [];

    for (let index in this) {
      const item = this[index];

      if ('function' === typeof item) {
        continue;
      }

      const text = ObjectInspector.wrap(item, newOptions).replace(/\n/g, replace);
      items.push(`${index}: ${text}`);
    }

    const text = items.join(`,${replace}`);

    if (isArray) {
      return ('' === text) ? '[]' : `[${replace}${text}\n]`;
    } else {
      return ('' === text) ? '{}' : `{${replace}${text}\n}`;
    }
  }

}

export default ObjectInspector;
