import {existsSync} from 'fs';
import Shell from '../Shell';

export type PackageManagerContructor = { new(): Base };

export interface Commands {
  addGlobalPackage: string;
  getGlobalModulePath: string;
  linkGlobalPackage: string;
}

export interface Package {
  link?: boolean;
  name: string;
  path?: string;
}

abstract class Base {
  readonly globalModulePath: String;
  protected readonly commands: Commands;
  protected readonly defaultPackage: Package = {
    name: '',
    path: '',
    link: false
  };

  // protected readonly isWindowsPlatform: boolean = ('win32' === process.platform);

  // noinspection TypeScriptAbstractClassConstructorCanBeMadeProtected
  constructor() {
    this.commands = this.getCommands();
    this.globalModulePath = this.getGlobalModulePath();

  }

  addGlobalPackage(item: string) {
    const command = `${this.commands.addGlobalPackage} ${item}`;
    return ('' !== item) && (false !== Shell.run(command));
  }

  // noinspection FunctionWithMultipleReturnPointsJS,JSUnusedGlobalSymbols
  getGlobalPackages(): object {
    const path = this.globalModulePath;

    if ('' === path) {
      return {};
    }

    // noinspection UnusedCatchParameterJS
    try {
      const item = require(path + '/../package.json');
      return item.hasOwnProperty('dependencies') ? item.dependencies : {};
    } catch (error) {
      return {};
    }

  }

  linkGlobalPackage(item: string) {
    const command = `${this.commands.linkGlobalPackage} ${item}`;
    return ('' !== item) && (false !== Shell.run(command));
  }

  // noinspection FunctionWithMultipleReturnPointsJS
  requireAllGlobalPackages(items: (Package | string)[]) {
    let result = true;

    for (let item of items) {
      if (false === result) {
        return false;
      }

      item = this.preparePackage(item);
      result = this.requireGlobalPackage(item.path!, item.name);

      if (result && item.link) {
        result = result && this.linkGlobalPackage(item.name);
      }
    }

    return result;
  }

  requireGlobalPackage(item: string, module: string = '') {
    const name = ('' === module) ? item : module;
    return this.existsGlobalPackagePath(name) ? true : this.addGlobalPackage(item);
  }

  protected existsGlobalPackagePath(name: string) {
    const path = this.globalModulePath;
    return (('' === path) || ('' === name)) ? false : existsSync(path + `/${name}`);
  }

  // noinspection JSMethodCanBeStatic
  protected getCommands(): Commands {
    return {
      getGlobalModulePath: '',
      addGlobalPackage: '',
      linkGlobalPackage: ''
    };
  }

  protected getGlobalModulePath(): string {
    let text = Shell.run(this.commands.getGlobalModulePath, {}, true);
    return (false === text) ? '' : text.toString();
  }

  // noinspection JSMethodCanBeStatic
  protected preparePackage(item: Package | string): Package {
    const source: Package = ('string' === typeof item) ? {name: item} : item;
    const result = Object.assign({}, this.defaultPackage, source);

    if ('' === result.path) {
      result.path = result.name;
    }

    return result;
  }

}

export default Base;
