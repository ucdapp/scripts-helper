import {realpathSync} from 'fs';
import Shell from '../../Shell';
import Base, {Commands} from '../Base';

class yarn extends Base {

  getCommands(): Commands {
    const items = super.getCommands();
    items.addGlobalPackage = 'yarn global add';
    items.getGlobalModulePath = 'yarn global dir';
    items.linkGlobalPackage = 'yarn link';
    return items;
  }

  linkGlobalPackage(item: string) {
    console.log(`linkGlobalPackage: ${item}`);
    return this.registerGlobalPackage(item) ? super.linkGlobalPackage(item) : false;
  }

  protected getGlobalModulePath() {
    return realpathSync(super.getGlobalModulePath() + '/node_modules');
  }

  protected registerGlobalPackage(item: string) {
    const path = `${this.globalModulePath}/${item}`;

    if (false === this.existsGlobalPackagePath(item)) {
      console.error('Error: "%s" is not exists.', item);
      return false;
    }

    const current = process.cwd();

    // noinspection UnusedCatchParameterJS
    try {
      process.chdir(path);
      const command = `${this.commands.linkGlobalPackage}`;
      return (false !== Shell.run(command));
    } catch (error) {
      console.error('Error: register "%s" global package fail.', item);
      return false;
    } finally {
      process.chdir(current);
    }
  }

}

export default yarn;
