import Base, {Commands} from '../Base';

class pnpm extends Base {

  getCommands(): Commands {
    const items = super.getCommands();
    items.addGlobalPackage = 'pnpm install --global';
    items.getGlobalModulePath = 'pnpm root --global';
    items.linkGlobalPackage = 'pnpm link';
    return items;
  }

}

export default pnpm;
