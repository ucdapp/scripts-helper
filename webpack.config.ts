import {resolve} from 'path';

module.exports = {
  mode: 'none',
  target: 'node',
  entry: './index.ts',
  module: {
    rules: [{
      test: /\.tsx?$/,
      use: 'ts-loader'
    }]
  },
  resolve: {extensions: ['.tsx', '.ts', '.js']},
  output: {
    filename: 'bundle.js',
    path: resolve(__dirname, '.webpack')
  }
};
