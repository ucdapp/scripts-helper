"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const fs_1 = require("fs");
const Shell_1 = tslib_1.__importDefault(require("../../Shell"));
const Base_1 = tslib_1.__importDefault(require("../Base"));
class yarn extends Base_1.default {
    getCommands() {
        const items = super.getCommands();
        items.addGlobalPackage = 'yarn global add';
        items.getGlobalModulePath = 'yarn global dir';
        items.linkGlobalPackage = 'yarn link';
        return items;
    }
    linkGlobalPackage(item) {
        console.log(`linkGlobalPackage: ${item}`);
        return this.registerGlobalPackage(item) ? super.linkGlobalPackage(item) : false;
    }
    getGlobalModulePath() {
        return fs_1.realpathSync(super.getGlobalModulePath() + '/node_modules');
    }
    registerGlobalPackage(item) {
        const path = `${this.globalModulePath}/${item}`;
        if (false === this.existsGlobalPackagePath(item)) {
            console.error('Error: "%s" is not exists.', item);
            return false;
        }
        const current = process.cwd();
        // noinspection UnusedCatchParameterJS
        try {
            process.chdir(path);
            const command = `${this.commands.linkGlobalPackage}`;
            return (false !== Shell_1.default.run(command));
        }
        catch (error) {
            console.error('Error: register "%s" global package fail.', item);
            return false;
        }
        finally {
            process.chdir(current);
        }
    }
}
exports.default = yarn;
